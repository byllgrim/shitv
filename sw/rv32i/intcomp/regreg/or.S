.global _start
_start:
	addi	x31, x0, 0



test_uplow:
	lui	x1, 0xF0000
	addi	x1, x1, 0xF
	lui	x2, 0x0F000
	addi	x2, x2, 0xF0
	or	x3, x1, x2
	lui	x4, 0xFF000
	addi	x4, x4, 0xFF
	beq	x3, x4, pass_uplow
fail_uplow:
	addi	x31, x0, 0xAA
	j	halt
pass_uplow:
	nop



halt:
	addi	x30, x0, 0x7FF
	sw	x31, 0(x30)
