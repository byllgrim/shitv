.global _start
_start:
	nop



test_two:
	addi	x1, x0, 1
	addi	x2, x0, 1
	add	x3, x1, x2
	addi	x4, x0, 2
	beq	x3, x4, pass_two
	j	fail_two
pass_two:
	addi	x1, x0, 0
	j	done_two
fail_two:
	addi	x1, x0, 0xAA
	j	halt
done_two:
	nop



test_evens:
	addi	x1, x0, 0
	addi	x2, x0, 2
	add	x1, x1, x2
	add	x1, x1, x2
	add	x1, x1, x2
	add	x1, x1, x2
	add	x1, x1, x2
	addi	x3, x0, 0x0A
	beq	x1, x3, pass_evens
	j	fail_evens
pass_evens:
	addi	x1, x0, 0
	j	done_evens
fail_evens:
	addi	x1, x0, 0xBB
	j	halt
done_evens:
	nop



halt:
	addi	x31, x0, 0x7FF
	sw	x1, 0(x31)
