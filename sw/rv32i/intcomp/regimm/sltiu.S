.global _start
_start:
	nop


test_zeroone:
	addi	x1, x0, 0
	sltiu	x2, x1, 1
	addi	x3, x0, 1
	beq	x2, x3, pass_zeroone
	j	fail_zeroone
pass_zeroone:
	addi	x1, x0, 0
	j	done_zeroone
fail_zeroone:
	addi	x1, x0, 0xAA
	j	halt
done_zeroone:
	nop


// TODO test 0xFFFFFFFF vs something


halt:
	addi	x31, x0, 0x7FF
	sw	x1, 0(x31)
