.global _start
_start:
	addi	x31, x0, 0


test_lsb:
	addi	x1, x0, 8
	srli	x2, x1, 1
	addi	x3, x0, 4
	beq	x2, x3, pass_lsb
fail_lsb:
	addi	x31, x0, 0xAA
	j	halt
pass_lsb:
	nop


test_msb:
	li	x1, 0x80000000
	srli	x2, x1, 1
	li	x3, 0x40000000
	beq	x2, x3, pass_msb
fail_msb:
	addi	x31, x0, 0xBB
	j	halt
pass_msb:
	nop


halt:
	addi	x30, x0, 0x7FF
	sw	x31, 0(x30)
