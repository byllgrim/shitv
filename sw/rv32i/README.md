Software tests for the base I instructions.

Directories here correspond to sections in the riscv's I chapter.
It does not try to be absolutely comprehensive.
Whenever other testing (e.g. riscv-compliance) reveals problems, the tests here
are updated to catch that case.
