# lint with "verible"
find ../../   \
| grep "\.v$" \
| xargs verilog_lint \
    --rules=-unpacked-dimensions-range-ordering,-explicit-parameter-storage-type
