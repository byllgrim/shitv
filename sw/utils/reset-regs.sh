cat $1 | grep ' reg '  \
\
| awk -F 'reg' '{print $2}'  \
| sed 's/\[.*\]//'           \
| sed 's/ *//'               \
| awk -F ';|,' '{print $1}'  \
\
| sed 's/^/        /'  \
| sed 's/$/ <= 0;/'
