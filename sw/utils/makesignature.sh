MEMDUMP=$1
ELF=$2
OUTFILE=$3

START=$(nm $ELF | grep signature_start | awk '{print $1}')
START=$(printf "0x%s" $START)
START=$(printf "%d" $START)
START=$((START / 4))
START=$((START + 1))

END=$(nm $ELF | grep signature_end | awk '{print $1}')
END=$(printf "0x%s" $END)
END=$(printf "%d" $END)
END=$((END / 4))

head -n  $END   $MEMDUMP      > tmp_signature
tail -n +$START tmp_signature > $OUTFILE
rm tmp_signature
