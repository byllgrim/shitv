# lint with "verible"
find ../../   \
| grep "\.v$" \
| xargs verilog_format --inplace
