# Find unregistered outputs and "r_" signals.

find ../../ | grep '\.v$' | xargs egrep ' r_| o_' | grep '=' | egrep -v '<=|=='

# find ../../ | grep '\.v$' | xargs grep always | grep '\*'
