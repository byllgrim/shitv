Here be files needed to run `riscv-compliance` with `shitv` as a target.

Usage:
1. Clone   `https://github.com/riscv/riscv-compliance`
2. Symlink `ln -s blabla/shitv/sw/compliance riscv-target/shitv`
3. Run     `make RISCV_TARGET=shitv SHITVDIR=blabla/shitv`
4. Check   `SUITEDIR=riscv-test-suite/rv32i WORK=work RISCV_ISA=rv32i ./riscv-test-env/verify.sh`
