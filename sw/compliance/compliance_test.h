#include "riscv_test.h"

#define RV_COMPLIANCE_RV32M \
	RVTEST_RV32M        \

#define RV_COMPLIANCE_CODE_BEGIN \
	.text                    \
	.global _start           \
	_start:                  \

#define RV_COMPLIANCE_CODE_END \

#define RV_COMPLIANCE_HALT  \
	li	x1, 0;      \
	li	x31, 0x7FF; \
	sw	x1, 0(x31); \

#define RV_COMPLIANCE_DATA_BEGIN \
	.align  4;               \
	.global signature_start; \
        signature_start:         \

#define RV_COMPLIANCE_DATA_END \
	.align  4;             \
	.global signature_end; \
        signature_end:         \
