`default_nettype none

module core (
    input wire clk,
    input wire rst,

    input  wire        i_ram_a_don,
    input  wire [31:0] i_ram_a_rdata,
    output wire        o_ram_a_rdy,
    output wire [31:0] o_ram_a_raddr,

    input  wire        i_ram_b_don,
    input  wire [31:0] i_ram_b_rdata,
    output wire        o_ram_b_rdy,
    output wire        o_ram_b_wen,
    output wire [ 2:0] o_ram_b_siz,
    output wire [31:0] o_ram_b_raddr,
    output wire [31:0] o_ram_b_waddr,
    output wire [31:0] o_ram_b_wdata
);



  // Register File

  wire [31:0] rf_rs1data;
  wire [31:0] rf_rs2data;

  regfile regfile (
      .clk(clk),
      .rst(rst),

      .i_a_raddr(idrf_rs1addr),
      .i_b_raddr(idrf_rs2addr),
      .i_waddr  (wb_waddr),
      .i_wdata  (wb_wdata),
      .i_we     (wb_we),

      .o_a_rdata(rf_rs1data),
      .o_b_rdata(rf_rs2data)
  );



  // IF Stage

  wire [31:0] if_instr;
  wire        if_valid;
  wire [31:0] if_pc;

  ifstage ifstage (
      .clk(clk),
      .rst(rst),

      .i_ex_ans(ex_ans),
      .i_ex_jmp(exif_jmp),

      .i_id_wait(idif_wait),

      .i_ram_don  (i_ram_a_don),
      .i_ram_rdata(i_ram_a_rdata),

      .o_id_instr(if_instr),
      .o_id_pc   (if_pc),
      .o_id_valid(if_valid),

      .o_ram_raddr(o_ram_a_raddr),
      .o_ram_rdy  (o_ram_a_rdy)
  );



  // ID Stage

  wire        id_jalr;
  wire        id_jmp;
  wire        id_useimm;
  wire        id_usepc;
  wire [31:0] id_imm;
  wire [31:0] id_pc;
  wire [ 4:0] id_rd;

  wire        idif_wait;
  wire        idwb_en;
  wire [ 4:0] idrf_rs1addr;
  wire [ 4:0] idrf_rs2addr;

  wire        idfwd_rs1mem;
  wire        idfwd_rs1ex;
  wire        idfwd_rs2mem;
  wire        idfwd_rs2ex;

  wire        idcond_eq;
  wire        idcond_ge;
  wire        idcond_geu;
  wire        idcond_lt;
  wire        idcond_ltu;
  wire        idcond_ne;

  wire        idmem_en;
  wire        idmem_signed;
  wire        idmem_wr;
  wire [ 2:0] idmem_siz;

  wire        idop_and;
  wire        idop_lt;
  wire        idop_ltu;
  wire        idop_or;
  wire        idop_sll;
  wire        idop_sra;
  wire        idop_srl;
  wire        idop_sub;
  wire        idop_xor;

  idstage idstage (
      .clk(clk),
      .rst(rst),

      .i_if_instr(if_instr),
      .i_if_pc   (if_pc),
      .i_if_valid(if_valid),

      .o_ex_jalr(id_jalr),
      .o_ex_jmp (id_jmp),
      .o_ex_pc  (id_pc),
      .o_ex_rd  (id_rd),

      .o_exalu_imm   (id_imm),
      .o_exalu_useimm(id_useimm),
      .o_exalu_usepc (id_usepc),

      .o_excond_eq (idcond_eq),
      .o_excond_ge (idcond_ge),
      .o_excond_geu(idcond_geu),
      .o_excond_lt (idcond_lt),
      .o_excond_ltu(idcond_ltu),
      .o_excond_ne (idcond_ne),

      .o_exfwd_rs1mem(idfwd_rs1mem),
      .o_exfwd_rs1ex (idfwd_rs1ex),
      .o_exfwd_rs2mem(idfwd_rs2mem),
      .o_exfwd_rs2ex (idfwd_rs2ex),

      .o_exmem_siz   (idmem_siz),
      .o_exmem_signed(idmem_signed),
      .o_exmem_en    (idmem_en),
      .o_exmem_wr    (idmem_wr),

      .o_exop_and(idop_and),
      .o_exop_lt (idop_lt),
      .o_exop_ltu(idop_ltu),
      .o_exop_or (idop_or),
      .o_exop_sll(idop_sll),
      .o_exop_sra(idop_sra),
      .o_exop_srl(idop_srl),
      .o_exop_sub(idop_sub),
      .o_exop_xor(idop_xor),

      .o_exwb_en(idwb_en),

      .o_if_wait(idif_wait),

      .o_rf_rs1(idrf_rs1addr),
      .o_rf_rs2(idrf_rs2addr)
  );



  // ID-EX extra step

  wire [31:0] w_idex_opa;
  wire [31:0] w_idex_opb;
  wire [31:0] w_idex_opc;

  idex idex (
      .i_ex_ans      (ex_ans),
      .i_id_jmp      (id_jmp),
      .i_id_pc       (id_pc),
      .i_id_useimm   (id_useimm),
      .i_id_usepc    (id_usepc),
      .i_idfwd_rs1ex (idfwd_rs1ex),
      .i_idfwd_rs1mem(idfwd_rs1mem),
      .i_idfwd_rs2ex (idfwd_rs2ex),
      .i_idfwd_rs2mem(idfwd_rs2mem),
      .i_idop_imm    (id_imm),
      .i_idop_rs1    (rf_rs1data),
      .i_idop_rs2    (rf_rs2data),
      .i_mem_data    (mem_data),

      .o_opa(w_idex_opa),
      .o_opb(w_idex_opb),
      .o_opc(w_idex_opc)
  );



  // EX Stage

  wire        exmem_en;
  wire        exmem_signed;
  wire        exmem_wben;
  wire        exmem_wr;
  wire [ 2:0] exmem_siz;
  wire [ 4:0] exmem_rd;
  wire [31:0] exmem_wdata;

  wire        exif_jmp;

  wire [31:0] ex_ans;

  exstage exstage (
      .clk(clk),
      .rst(rst),

      .i_id_jalr(id_jalr),
      .i_id_jmp (id_jmp),
      .i_id_pc  (id_pc),
      .i_id_rd  (id_rd),

      .i_idcond_eq (idcond_eq),
      .i_idcond_ge (idcond_ge),
      .i_idcond_geu(idcond_geu),
      .i_idcond_lt (idcond_lt),
      .i_idcond_ltu(idcond_ltu),
      .i_idcond_ne (idcond_ne),

      .i_idmem_siz   (idmem_siz),
      .i_idmem_signed(idmem_signed),
      .i_idmem_en    (idmem_en),
      .i_idmem_wr    (idmem_wr),

      .i_idop_and(idop_and),
      .i_idop_lt (idop_lt),
      .i_idop_ltu(idop_ltu),
      .i_idop_or (idop_or),
      .i_idop_sll(idop_sll),
      .i_idop_sra(idop_sra),
      .i_idop_srl(idop_srl),
      .i_idop_sub(idop_sub),
      .i_idop_xor(idop_xor),

      .i_opa(w_idex_opa),
      .i_opb(w_idex_opb),
      .i_opc(w_idex_opc),

      .i_idwb_en(idwb_en),

      .o_ans(ex_ans),

      .o_if_jmp(exif_jmp),

      .o_mem_siz   (exmem_siz),
      .o_mem_en    (exmem_en),
      .o_mem_rd    (exmem_rd),
      .o_mem_wb    (exmem_wben),
      .o_mem_signed(exmem_signed),
      .o_mem_wdata (exmem_wdata),
      .o_mem_wr    (exmem_wr)
  );



  // MEM Stage

  wire        mem_en;
  wire [31:0] mem_data;
  wire [ 4:0] mem_rd;
  wire        mem_signed;
  wire        mem_ramread;

  memstage memstage (
      .clk(clk),
      .rst(rst),

      .i_ex_ans   (ex_ans),
      .i_ex_en    (exmem_en),
      .i_ex_siz   (exmem_siz),
      .i_ex_rd    (exmem_rd),
      .i_ex_signed(exmem_signed),
      .i_ex_wb    (exmem_wben),
      .i_ex_wdata (exmem_wdata),
      .i_ex_wr    (exmem_wr),

      .o_ram_raddr(o_ram_b_raddr),
      .o_ram_waddr(o_ram_b_waddr),
      .o_ram_wdata(o_ram_b_wdata),
      .o_ram_rdy  (o_ram_b_rdy),
      .o_ram_siz  (o_ram_b_siz),
      .o_ram_wen  (o_ram_b_wen),

      .o_wb_data   (mem_data),
      .o_wb_en     (mem_en),
      .o_wb_signed (mem_signed),
      .o_wb_ramread(mem_ramread),
      .o_wb_rd     (mem_rd)
  );



  // WB Stage

  wire        wb_we;
  wire [ 4:0] wb_waddr;
  wire [31:0] wb_wdata;

  wbstage wbstage (
      .clk(clk),
      .rst(rst),

      .i_mem_data   (mem_data),
      .i_mem_en     (mem_en),
      .i_mem_ramread(mem_ramread),
      .i_mem_signed (mem_signed),
      .i_mem_siz    (o_ram_b_siz),
      .i_mem_rd     (mem_rd),

      .i_ram_rdata(i_ram_b_rdata),
      .i_ram_don  (i_ram_b_don),

      .o_rf_waddr(wb_waddr),
      .o_rf_wdata(wb_wdata),
      .o_rf_we   (wb_we)
  );
endmodule
