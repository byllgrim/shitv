`default_nettype none

module regfile (
    input wire clk,
    input wire rst,

    input wire [ 4:0] i_a_raddr,
    input wire [ 4:0] i_b_raddr,
    input wire [ 4:0] i_waddr,
    input wire [31:0] i_wdata,
    input wire        i_we,

    output wire [31:0] o_a_rdata,
    output wire [31:0] o_b_rdata
);

  reg [31:0] registers[0:31];


  // Serve read data
  assign o_a_rdata = (i_a_raddr == 0) ? 0 : registers[i_a_raddr];
  assign o_b_rdata = (i_b_raddr == 0) ? 0 : registers[i_b_raddr];


  integer i;

  always @(posedge clk) begin : write2regs
    if (rst) begin
      for (i = 0; i < 32; i = i + 1) registers[i] <= 0;
    end else if (i_we) begin
      registers[i_waddr] <= i_wdata;
    end
  end

endmodule
