# Overview of Modules

(this was written during development and may not be accurate anymore)



## Core

Top level of shitv is `core.v`.
Only instantiates and connects modules, no combinatorial logic.
RAM is not part of shitv.

It implements a classic 5-stage RISC pipeline:

    IF -> ID -> EX -> MEM -> WB



## Register File

General purpose registers (GPR), x0 to x31, resides in the module `regfile`.

Regfile is _not_ contained within ID-stage, even if this would traditionally be
expected.
It is placed _after_ ID-stage in order to maintain the philosophical purity of
the ID-stage.

Writes are synchronous, but reads are not.



## IF-Stage (Instruction Fetch)

Fetches instructions from instruction memory.
Currently, there is no prefetch or cache.



## ID-Stage (Instruction Decode)

Receives instructions from IF-stage.

First, it extracts instruction subfields:

    opcode, rd, imm, funct3, rs1, rs2, funct7

Second, it determines instruction "type":

    Integer computational
    Control transfer
    Load and store

Finally, passing relevant information to the EX-stage.



## Other Stages

Read the source code.
