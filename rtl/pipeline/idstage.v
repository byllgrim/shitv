`default_nettype none

module idstage (
    input wire clk,
    input wire rst,

    input  wire [31:0] i_if_instr,
    input  wire        i_if_valid,
    input  wire [31:0] i_if_pc,
    output reg         o_if_wait,

    output reg        o_ex_jalr,
    output reg        o_ex_jmp,
    output reg [31:0] o_ex_pc,
    output reg [ 4:0] o_ex_rd,

    output reg [31:0] o_exalu_imm,
    output reg        o_exalu_usepc,
    output reg        o_exalu_useimm,

    output reg o_excond_eq,
    output reg o_excond_ne,
    output reg o_excond_lt,
    output reg o_excond_ge,
    output reg o_excond_ltu,
    output reg o_excond_geu,

    output reg o_exop_and,
    output reg o_exop_lt,
    output reg o_exop_ltu,
    output reg o_exop_or,
    output reg o_exop_sll,
    output reg o_exop_sra,
    output reg o_exop_srl,
    output reg o_exop_sub,
    output reg o_exop_xor,

    output reg o_exfwd_rs1ex,
    output reg o_exfwd_rs1mem,
    output reg o_exfwd_rs2ex,
    output reg o_exfwd_rs2mem,

    output reg [2:0] o_exmem_siz,
    output reg       o_exmem_signed,
    output reg       o_exmem_en,
    output reg       o_exmem_wr,

    output reg o_exwb_en,

    output reg [4:0] o_rf_rs1,
    output reg [4:0] o_rf_rs2
);

  // Extract instruction subfields

  wire [6:0] opcode = i_if_instr[6:0];
  wire [4:0] rd = i_if_instr[11:7];
  wire [2:0] funct3 = i_if_instr[14:12];
  wire [6:0] funct7 = i_if_instr[31:25];
  wire [4:0] rs1 = i_if_instr[19:15];
  wire [4:0] rs2 = i_if_instr[24:20];
  wire [31:0] imm12s = {{20{i_if_instr[31]}}, i_if_instr[31:25], i_if_instr[11:7]};
  wire [31:0] imm12b = {
                             {20 {i_if_instr[31]} },
                             i_if_instr[7],
                             i_if_instr[30:25],
                             i_if_instr[11:8],
                             1'b0
                         };
  wire [31:0] imm20u = {i_if_instr[31:12], 12'b0};
  wire [31:0] imm20j = {
                             {12 {i_if_instr[31]} },
                             i_if_instr[19:12],
                             i_if_instr[20],
                             i_if_instr[30:21],
                             1'b0
                         };
  wire [31:0] imm12i = {{20{i_if_instr[31]}}, i_if_instr[31:20]};
  wire [4:0] shamt = i_if_instr[24:20];


  // Identify opcode

  wire opc_load = (opcode == 7'b00_000_11);
  wire opc_opimm = (opcode == 7'b00_100_11);
  wire opc_auipc = (opcode == 7'b00_101_11);

  wire opc_store = (opcode == 7'b01_000_11);
  wire opc_op = (opcode == 7'b01_100_11);
  wire opc_lui = (opcode == 7'b01_101_11);

  wire opc_branch = (opcode == 7'b11_000_11);
  wire opc_jalr = (opcode == 7'b11_001_11);
  wire opc_jal = (opcode == 7'b11_011_11);


  // Recognize instructions: Integer Computational (imm)

  wire ins_addi = (opc_opimm && (funct3 == 3'b000));
  wire ins_xori = (opc_opimm && (funct3 == 3'b100));
  wire ins_ori = (opc_opimm && (funct3 == 3'b110));
  wire ins_andi = (opc_opimm && (funct3 == 3'b111));

  wire ins_slti = (opc_opimm && (funct3 == 3'b010));
  wire ins_sltiu = (opc_opimm && (funct3 == 3'b011));

  wire ins_slli = (opc_opimm && (funct3 == 3'b001));
  wire ins_srli = (opc_opimm && (funct3 == 3'b101) && (funct7 == 0));
  wire ins_srai = (opc_opimm && (funct3 == 3'b101) && (funct7 == 32));

  wire ins_lui = opc_lui;
  wire ins_auipc = opc_auipc;


  // Recognize instructions: Integer Computational (reg)

  wire ins_add = (opc_op && (funct3 == 3'b000) && (funct7 == 0));
  wire ins_sub = (opc_op && (funct3 == 3'b000) && (funct7 == 32));
  wire ins_xor = (opc_op && (funct3 == 3'b100));
  wire ins_or = (opc_op && (funct3 == 3'b110));
  wire ins_and = (opc_op && (funct3 == 3'b111));

  wire ins_slt = (opc_op && (funct3 == 3'b010));
  wire ins_sltu = (opc_op && (funct3 == 3'b011));

  wire ins_sll = (opc_op && (funct3 == 3'b001));
  wire ins_srl = (opc_op && (funct3 == 3'b101) && (funct7 == 0));
  wire ins_sra = (opc_op && (funct3 == 3'b101) && (funct7 == 32));

  wire w_rtype = ins_add | ins_sub | ins_sll | ins_slt | ins_sltu | ins_xor | ins_srl | ins_sra |
      ins_or | ins_and;


  // Recognize instructions: Control Transfer

  wire ins_jal = opc_jal;
  wire ins_jalr = opc_jalr;

  wire ins_beq = (opc_branch && (funct3 == 3'b000));
  wire ins_bne = (opc_branch && (funct3 == 3'b001));
  wire ins_blt = (opc_branch && (funct3 == 3'b100));
  wire ins_bge = (opc_branch && (funct3 == 3'b101));
  wire ins_bltu = (opc_branch && (funct3 == 3'b110));
  wire ins_bgeu = (opc_branch && (funct3 == 3'b111));


  // Recognize instructions: Load and Store

  wire ins_lb = (opc_load && (funct3 == 3'b000));
  wire ins_lh = (opc_load && (funct3 == 3'b001));
  wire ins_lw = (opc_load && (funct3 == 3'b010));
  wire ins_lbu = (opc_load && (funct3 == 3'b100));
  wire ins_lhu = (opc_load && (funct3 == 3'b101));

  wire ins_sb = (opc_store && (funct3 == 3'b000));
  wire ins_sh = (opc_store && (funct3 == 3'b001));
  wire ins_sw = (opc_store && (funct3 == 3'b010));

  wire w_store = ins_sb | ins_sh | ins_sw;


  // Drive controls to ex-stage

  always @(posedge clk) begin
    o_if_wait <= 0;
    o_ex_jalr <= 0;
    o_ex_jmp <= 0;
    o_ex_pc <= 0;
    o_ex_rd <= 0;
    o_exalu_imm <= 0;
    o_exalu_usepc <= 0;
    o_exalu_useimm <= 0;
    o_excond_eq <= 0;
    o_excond_ne <= 0;
    o_excond_lt <= 0;
    o_excond_ge <= 0;
    o_excond_ltu <= 0;
    o_excond_geu <= 0;
    o_exop_and <= 0;
    o_exop_lt <= 0;
    o_exop_ltu <= 0;
    o_exop_or <= 0;
    o_exop_sll <= 0;
    o_exop_sra <= 0;
    o_exop_srl <= 0;
    o_exop_sub <= 0;
    o_exop_xor <= 0;
    o_exfwd_rs1ex <= 0;
    o_exfwd_rs1mem <= 0;
    o_exfwd_rs2ex <= 0;
    o_exfwd_rs2mem <= 0;
    o_exmem_en <= 0;
    o_exmem_siz <= 0;
    o_exmem_signed <= 0;
    o_exmem_wr <= 0;
    o_exwb_en <= 0;
    o_rf_rs1 <= 0;
    o_rf_rs2 <= 0;
    r_ins_unknown <= 0;
    r_exrd_d1 <= 0;
    r_memhazard_d <= 0;
    r_uncondjmp_d1 <= 0;
    r_uncondjmp_d2 <= 0;

    if (!rst && i_if_valid) begin
      detectmemhazard;
      if (!w_memhazard && (r_memhazard_d == 0) && !r_uncondjmp_d1 && !r_uncondjmp_d2) begin
        controlexstage;
        selectforwarding;
      end
    end
  end


  // Calculate controls to ex-stage

  reg r_ins_unknown;  // TODO wtf_opcode, wtf_intcomp, etc?

  task automatic controlexstage;
    begin
      o_ex_pc <= i_if_pc;
      o_exalu_useimm <= !w_rtype;
      o_exmem_wr <= w_store;
      o_rf_rs1 <= (ins_lui | ins_auipc | ins_jal) ? 0 : rs1;

      if (ins_addi | ins_srli | ins_srai | ins_slli | ins_slti | ins_sltiu
            | ins_xori | ins_andi | ins_lui | ins_auipc | ins_ori | ins_add
            | ins_and | ins_sra | ins_or | ins_sll | ins_srl | ins_xor
            | ins_slt | ins_sltu | ins_sub
           )
            intcomp;
        else if (ins_jal | ins_jalr | ins_beq | ins_bne | ins_blt | ins_bge
                 | ins_bltu | ins_bgeu
                )
            ctrltran;
        else if (ins_sw | ins_sh | ins_sb
                 | ins_lw | ins_lh | ins_lb
                 | ins_lhu | ins_lbu
                )
            loadstore;
        else
            r_ins_unknown <= 1;

      if (r_ins_unknown)  // TODO better way to warn about this?
          ;
    end
  endtask


  // Calculate Integer Computational

  task automatic intcomp;
    begin
      o_ex_rd <= rd;
      o_exwb_en <= 1;

      if (ins_addi) begin
        o_exalu_imm <= imm12i;
      end else if (ins_lui) begin
        o_exalu_imm <= imm20u;
      end else if (ins_auipc) begin
        o_exalu_imm <= imm20u;
        o_exalu_usepc <= 1;
      end else if (ins_slti) begin
        o_exop_lt <= 1;
        o_exalu_imm <= imm12i;  // TODO DRY
      end else if (ins_sltiu) begin
        o_exop_ltu <= 1;
        o_exalu_imm <= imm12i;  // TODO DRY
      end else if (ins_ori) begin
        o_exop_or <= 1;
        o_exalu_imm <= imm12i;  // TODO DRY
      end else if (ins_xori) begin
        o_exop_xor <= 1;
        o_exalu_imm <= imm12i;  // TODO DRY
      end else if (ins_slli) begin
        o_exop_sll <= 1;
        o_exalu_imm <= {27'd0, shamt};  // TODO DRY
      end else if (ins_srli) begin
        o_exop_srl <= 1;
        o_exalu_imm <= {27'd0, shamt};  // TODO DRY
      end else if (ins_srai) begin
        o_exop_sra <= 1;
        o_exalu_imm <= {27'd0, shamt};  // TODO DRY
      end else if (ins_andi) begin
        o_exop_and <= 1;
        o_exalu_imm <= imm12i;  // TODO DRY
      end else if (ins_add) begin
        o_rf_rs2 <= rs2;
      end else if (ins_sub) begin
        o_exop_sub <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_and) begin
        o_exop_and <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_sra) begin
        o_exop_sra <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_or) begin
        o_exop_or <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_sll) begin
        o_exop_sll <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_srl) begin
        o_exop_srl <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_slt) begin  // TODO not both!
        o_exop_lt <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_sltu) begin  // TODO not both!
        o_exop_ltu <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_xor) begin
        o_exop_xor <= 1;
        o_rf_rs2 <= rs2;
      end
    end
  endtask


  // Calculate Control Transfer

  task automatic ctrltran;
    begin
      o_ex_jmp <= 1;

      if (ins_jal) begin
        o_exalu_imm <= imm20j;
      end else if (ins_jalr) begin
        o_exalu_imm <= imm12i;
        o_ex_jalr <= 1;
      end else if (ins_beq) begin
        o_exalu_imm <= imm12b;
        o_excond_eq <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_bne) begin
        o_exalu_imm <= imm12b;
        o_excond_ne <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_blt) begin
        o_exalu_imm <= imm12b;
        o_excond_lt <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_bge) begin
        o_exalu_imm <= imm12b;
        o_excond_ge <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_bltu) begin
        o_exalu_imm <= imm12b;
        o_excond_ltu <= 1;
        o_rf_rs2 <= rs2;
      end else if (ins_bgeu) begin
        o_exalu_imm <= imm12b;
        o_excond_geu <= 1;
        o_rf_rs2 <= rs2;
      end
      // TODO refactor "common denominators"?
    end
  endtask


  // Calculate Load and Store

  task automatic loadstore;
    begin
      o_exmem_en <= 1;

      if (ins_sw) begin
        o_exmem_siz <= 4;
        o_rf_rs2 <= rs2;
        o_exalu_imm <= imm12s;
      end else if (ins_sh) begin
        o_exmem_siz <= 2;
        o_rf_rs2 <= rs2;
        o_exalu_imm <= imm12s;
      end else if (ins_sb) begin
        o_exmem_siz <= 1;
        o_rf_rs2 <= rs2;
        o_exalu_imm <= imm12s;
      end else if (ins_lw) begin
        o_exmem_siz <= 4;
        o_ex_rd <= rd;
        o_exalu_imm <= imm12i;
        // TODO loads from x0 should be an exception?
      end else if (ins_lh) begin
        o_exmem_siz <= 2;
        o_exmem_signed <= 1;
        o_ex_rd <= rd;
        o_exalu_imm <= imm12i;
      end else if (ins_lb) begin
        o_exmem_siz <= 1;
        o_exmem_signed <= 1;
        o_ex_rd <= rd;
        o_exalu_imm <= imm12i;
      end else if (ins_lhu) begin
        o_exmem_siz <= 2;  // TODO is this smart?
        o_ex_rd <= rd;
        o_exalu_imm <= imm12i;
      end else if (ins_lbu) begin
        o_exmem_siz <= 1;  // TODO is this smart?
        o_ex_rd <= rd;
        o_exalu_imm <= imm12i;
      end
    end
  endtask


  // Selecting operand sources

  reg                                   [4:0] r_exrd_d1;
  wire w_utype = (ins_lui | ins_auipc);
  wire w_jtype = ins_jal;
  // TODO w_itype for rs2?

  task automatic selectforwarding;
    begin
      if (!w_utype && !w_jtype) setforwarding;

      r_exrd_d1 <= o_ex_rd;
    end
  endtask

  task automatic setforwarding;
    begin
      if ((rs1 == o_ex_rd) && (rs1 != 0))
            o_exfwd_rs1ex <= 1;
        else if ((rs1 == r_exrd_d1) && (rs1 != 0))
            o_exfwd_rs1mem <= 1;

      if ((rs2 == o_ex_rd) && (rs2 != 0))
            o_exfwd_rs2ex <= 1;
        else if ((rs2 == r_exrd_d1) && (rs2 != 0))
            o_exfwd_rs2mem <= 1;
    end
  endtask


  wire w_memhazard = (o_exmem_en && !o_exmem_wr);
  wire w_uncondjmp = (ins_jal || ins_jalr);
  reg                                             [3:0] r_memhazard_d;  // TODO why 4 bits?
  reg                                                   r_uncondjmp_d1;
  reg                                                   r_uncondjmp_d2;

  task automatic detectmemhazard;
    begin
      r_memhazard_d[3] <= w_memhazard;
      r_memhazard_d[2:0] <= r_memhazard_d[3:1];
      r_uncondjmp_d1 <= w_uncondjmp;
      r_uncondjmp_d2 <= r_uncondjmp_d1;

      if (w_memhazard || (r_memhazard_d != 0)) begin
        o_if_wait <= 1;
      end
    end
  endtask

endmodule
