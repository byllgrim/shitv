`default_nettype none

// TODO split in fetcher (to ram) and messenger (to ID)

module ifstage (
    input wire clk,
    input wire rst,

    input wire [31:0] i_ex_ans,
    input wire        i_ex_jmp,

    input  wire [31:0] i_ram_rdata,
    input  wire        i_ram_don,
    output reg  [31:0] o_ram_raddr,  // TODO why: extra cycle after r_pc?
    output reg         o_ram_rdy,

    input  wire        i_id_wait,
    output reg  [31:0] o_id_instr,
    output reg  [31:0] o_id_pc,
    output reg         o_id_valid
);

  reg        r_id_wait_d1;
  reg [31:0] r_pc;
  reg [31:0] r_pc_d1;


  always @(posedge clk) begin : driveoutputs
    o_ram_raddr <= 0;
    o_ram_rdy <= 0;
    o_id_instr <= 0;
    o_id_pc <= 0;
    o_id_valid <= 0;
    r_id_wait_d1 <= 0;
    r_pc <= 0;
    r_pc_d1 <= 0;

    if (!rst) begin
      r_id_wait_d1 <= i_id_wait;

      if (!i_id_wait) begin
        r_pc_d1 <= o_ram_raddr;  // TODO why names no match?
        incrementpc;
        getfromram;
        feedidstage;
      end else begin
        waitforid;
      end
    end
  end


  task automatic incrementpc;
    begin
      r_pc <= r_pc + 4;  // TODO how to know if actually ready to proceed?

      if (i_ex_jmp) r_pc <= i_ex_ans[31:0];
      // TODO represent jumps in 'valid' signal?
    end
  endtask


  task automatic getfromram;
    begin
      if (!i_ex_jmp && !i_id_wait) begin
        o_ram_raddr <= {r_pc[31:2], 2'b00};  // TODO rename pc -> pc_next?
        o_ram_rdy <= 1;
        // TODO named tmp variable: pc_IALIGN?
        // TODO review if logic can be simplified
      end
    end
  endtask


  task automatic feedidstage;
    begin
      if (!i_ex_jmp && i_ram_don && o_ram_rdy) begin
        // TODO make conditions clearer to understand
        o_id_instr <= i_ram_rdata;
        o_id_valid <= 1;
        o_id_pc <= r_pc_d1;
      end
    end
  endtask


  task automatic waitforid;
    begin
      r_pc <= r_pc_d1 - 8;

      if (r_id_wait_d1) begin
        // TODO disable when ram says "s_don"
        r_pc <= r_pc;
        getfromram;
      end
    end
  endtask

endmodule
