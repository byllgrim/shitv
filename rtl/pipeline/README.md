(this document was an aid during development and might be wrong now)



## This Directory

Source files for the stages of a classic risc pipeline:
`IF -> ID -> EX -> MEM -> WB`



## Selecting Operands for EX


### Operand Sources

* imm
* rs1
* rs2
* pc


### Instruction Encodings

```
  imm rs1 rs2 pc
R       1   1  ?
I   1   1      ?
S   1   1   1
B   1   1   1  1
U   1          1
J   1          1
```


### Possible Pairings

```
    imm rs1 rs2 pc
imm   -   1   ?  1
rs1   -   -   1  ?
rs2   -   -   -  ?
pc    -   -   -  -
```

Is it better to send opa/opb/opc/pc, or send imm/rs1/rs2/pc?

There aren't that many combinations?
One could use signals to tell EX e.g. "use rs1/rs2 now" or "use imm/rs1"?

But if one sends rs1/etc, then EX must be a decoder/controller itself.
Preferably ID does all of decoding and controlling.
