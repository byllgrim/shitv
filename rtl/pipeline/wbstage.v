`default_nettype none

module wbstage (
    input wire clk,
    input wire rst,

    input wire        i_mem_en,
    input wire [31:0] i_mem_data,
    input wire [ 4:0] i_mem_rd,
    input wire        i_mem_ramread,
    input wire        i_mem_signed,
    input wire [ 2:0] i_mem_siz,

    input wire [31:0] i_ram_rdata,
    input wire        i_ram_don,

    output wire        o_rf_we,
    output wire [ 4:0] o_rf_waddr,
    output wire [31:0] o_rf_wdata
);

  reg r_mem_signed_d1;
  reg [2:0] r_mem_siz_d1;
  reg [4:0] r_mem_rd_d1;

  wire w_wb_request = (i_mem_en && !i_mem_ramread);


  // Sign extension for ram

  wire w_need_ext = (i_mem_signed | r_mem_signed_d1);
  wire [31:0] w_rdata_halfext = {{16{i_ram_rdata[15]}}, i_ram_rdata[15:0]};
  wire [31:0] w_rdata_byteext = {{24{i_ram_rdata[7]}}, i_ram_rdata[7:0]};
  wire [31:0] w_rdata_ext = (w_need_ext && (r_mem_siz_d1 == 1)) ?
      w_rdata_byteext : (w_need_ext && (r_mem_siz_d1 == 2)) ? w_rdata_halfext : i_ram_rdata;


  // Save data into regfile

  assign o_rf_we = (i_ram_don || w_wb_request);
  assign o_rf_waddr = (w_wb_request) ? i_mem_rd : (i_ram_don) ? r_mem_rd_d1 : 0;
  assign o_rf_wdata = (w_wb_request) ? i_mem_data : (i_ram_don) ? w_rdata_ext : 0;


  always @(posedge clk) begin : clock_registers
    r_mem_rd_d1 <= 0;
    r_mem_signed_d1 <= 0;
    r_mem_siz_d1 <= 0;

    if (!rst) begin
      r_mem_rd_d1 <= i_mem_rd;
      r_mem_signed_d1 <= i_mem_signed;
      r_mem_siz_d1 <= i_mem_siz;
    end
  end

endmodule
