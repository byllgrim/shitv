`default_nettype none

module exstage (
    input wire clk,
    input wire rst,

    input wire [31:0] i_id_pc,
    input wire [ 4:0] i_id_rd,
    input wire        i_id_jalr,
    input wire        i_id_jmp,

    input wire i_idwb_en,

    input wire i_idcond_eq,
    input wire i_idcond_ge,
    input wire i_idcond_geu,
    input wire i_idcond_lt,
    input wire i_idcond_ltu,
    input wire i_idcond_ne,

    input wire [2:0] i_idmem_siz,
    input wire       i_idmem_signed,
    input wire       i_idmem_en,
    input wire       i_idmem_wr,

    input wire i_idop_and,
    input wire i_idop_lt,
    input wire i_idop_ltu,
    input wire i_idop_or,
    input wire i_idop_sll,
    input wire i_idop_sra,
    input wire i_idop_srl,
    input wire i_idop_sub,
    input wire i_idop_xor,

    input wire [31:0] i_opa,
    input wire [31:0] i_opb,
    input wire [31:0] i_opc,

    output reg [31:0] o_ans,

    output reg [31:0] o_mem_wdata,
    output reg [ 4:0] o_mem_rd,
    output reg [ 2:0] o_mem_siz,
    output reg        o_mem_signed,
    output reg        o_mem_en,
    output reg        o_mem_wb,
    output reg        o_mem_wr,

    output reg o_if_jmp
);

  reg r_if_jmp_d1;

  always @(posedge clk) begin : registeroutputs
    o_mem_wdata <= 0;
    o_mem_en <= 0;
    o_mem_siz <= 0;
    o_mem_signed <= 0;
    o_mem_wr <= 0;
    o_mem_rd <= 0;
    o_mem_wb <= 0;
    o_ans <= 0;
    o_if_jmp <= 0;
    r_if_jmp_d1 <= 0;

    if (!rst) begin
      r_if_jmp_d1 <= o_if_jmp;

      if (!o_if_jmp && !r_if_jmp_d1) begin
        controlmemstage;
        controljumps;
        calculateanswer;
      end
    end
  end


  task automatic controlmemstage;
    begin
      o_mem_wdata <= i_opc;
      o_mem_en <= i_idmem_en;
      o_mem_siz <= i_idmem_siz;
      o_mem_signed <= i_idmem_signed;
      o_mem_wr <= i_idmem_wr;
      o_mem_rd <= i_id_rd;
      o_mem_wb <= i_idwb_en;
    end
  endtask


  task automatic controljumps;
    begin
      o_if_jmp <= i_id_jmp;

      if (i_idcond_eq) o_if_jmp <= (i_opb == i_opc);
      if (i_idcond_ne) o_if_jmp <= (i_opb != i_opc);
      if (i_idcond_lt) o_if_jmp <= ($signed(i_opb) < $signed(i_opc));
      if (i_idcond_ltu) o_if_jmp <= (i_opb < i_opc);
      if (i_idcond_ge) o_if_jmp <= ($signed(i_opb) >= $signed(i_opc));
      if (i_idcond_geu) o_if_jmp <= (i_opb >= i_opc);
    end
  endtask


  task automatic calculateanswer;
    begin
      o_ans <= i_opb + i_opa;

      if (i_idop_lt) o_ans <= {31'd0, ($signed(i_opb) < $signed(i_opa))};
      // TODO why reversed?
      if (i_idop_ltu) o_ans <= {31'd0, (i_opb < i_opa)};
      if (i_idop_or) o_ans <= i_opb | i_opa;
      if (i_idop_xor) o_ans <= i_opb ^ i_opa;
      if (i_idop_and) o_ans <= i_opb & i_opa;
      if (i_idop_sll) o_ans <= i_opb << i_opa;
      if (i_idop_srl) o_ans <= i_opb >> i_opa;
      if (i_idop_sra) o_ans <= $signed(i_opb) >>> i_opa;
      if (i_idop_sub) o_ans <= i_opb - i_opa;

      if (i_id_jmp && !i_id_jalr) o_ans <= i_id_pc + i_opa;
    end
  endtask

endmodule
