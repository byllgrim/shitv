`default_nettype none

module memstage (
    input wire clk,
    input wire rst,

    input wire [31:0] i_ex_ans,
    input wire        i_ex_en,
    input wire [ 2:0] i_ex_siz,
    input wire [ 4:0] i_ex_rd,
    input wire        i_ex_signed,
    input wire        i_ex_wb,
    input wire [31:0] i_ex_wdata,
    input wire        i_ex_wr,

    output reg [31:0] o_ram_raddr,
    output reg [31:0] o_ram_waddr,
    output reg [31:0] o_ram_wdata,
    output reg [ 2:0] o_ram_siz,
    output reg        o_ram_rdy,
    output reg        o_ram_wen,

    output reg [31:0] o_wb_data,
    output reg        o_wb_en,
    output reg        o_wb_signed,
    output reg        o_wb_ramread,
    output reg [ 4:0] o_wb_rd
);

  wire w_load = (i_ex_en && !i_ex_wr);
  wire w_store = (i_ex_en && i_ex_wr);


  always @(posedge clk) begin : driveoutputs
    o_ram_raddr <= 0;
    o_ram_waddr <= 0;
    o_ram_wdata <= 0;
    o_ram_wen <= 0;
    o_wb_data <= 0;
    o_wb_en <= 0;
    o_wb_ramread <= 0;
    o_wb_signed <= 0;
    o_wb_rd <= 0;
    o_ram_rdy <= 0;
    o_ram_siz <= 0;

    if (!rst) begin
      controlram;
      controlwb;
    end
  end


  task automatic controlram;
    begin
      if (w_store) begin
        o_ram_rdy <= 1;
        o_ram_waddr <= i_ex_ans;
        o_ram_siz <= i_ex_siz;

        o_ram_wen <= 1;
        o_ram_wdata <= i_ex_wdata;

      end else if (w_load) begin
        o_ram_rdy <= 1;
        o_ram_raddr <= i_ex_ans;
        o_ram_siz <= i_ex_siz;
      end
    end
  endtask


  task automatic controlwb;
    begin
      o_wb_en <= (w_load || i_ex_wb);
      o_wb_rd <= i_ex_rd;
      o_wb_data <= i_ex_ans;
      o_wb_ramread <= w_load;
      o_wb_signed <= i_ex_signed;
    end
  endtask

endmodule
