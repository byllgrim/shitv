`default_nettype none

module idex (
    input wire        i_id_jmp,
    input wire        i_id_useimm,
    input wire        i_id_usepc,
    input wire        i_idfwd_rs1ex,
    input wire        i_idfwd_rs1mem,
    input wire        i_idfwd_rs2ex,
    input wire        i_idfwd_rs2mem,
    input wire [31:0] i_ex_ans,
    input wire [31:0] i_id_pc,
    input wire [31:0] i_idop_imm,
    input wire [31:0] i_idop_rs1,
    input wire [31:0] i_idop_rs2,
    input wire [31:0] i_mem_data,

    output wire [31:0] o_opa,
    output wire [31:0] o_opb,
    output wire [31:0] o_opc
);

  assign o_opa = (i_id_jmp) ? i_idop_imm : (!i_id_useimm) ? o_opc : i_idop_imm;
  assign o_opb = (i_id_usepc) ?
      i_id_pc : (i_idfwd_rs1mem) ? i_mem_data : (i_idfwd_rs1ex) ? i_ex_ans : i_idop_rs1;
  assign o_opc = (i_idfwd_rs2mem) ? i_mem_data : (i_idfwd_rs2ex) ? i_ex_ans : i_idop_rs2;

endmodule
