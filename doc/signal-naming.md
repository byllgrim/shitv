Currently, there is no pervasive scheme.



## Proposal

The closest resemblance of the current "scheme":

`type [_group] _name [_suffix]`

Type:

    i input
    o output
    r reg
    w wire

Group (examples):

    ram
    exfwd
    wb
    idop

Name (examples):

    en
    wdata
    don

Suffix:

    _d1 delayed 1 cycle
    _d2 delayed 2 cycle
    ...



## Alternative

`group_type_name_suffix`

Tho I saw someone advice against not having io at either end or beginning.
