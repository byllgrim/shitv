## Signal Description

* m_rdy - master output ready for use
* m_out - master payload (e.g. m_dat, m_adr, m_wr, ...)
* s_rdy - slave accepts order (if any)
* s_out - slave payload (e.g. s_dat, ...)
* s_don - slave payload is valid



## Transaction Phases

A transaction (a single read or write), is split in phases.

Phase sequence:
`-> [idl] -> beg -> [hld] -> end ->`

Phase sequence graph: (http://webgraphviz.com/)
```
digraph G {
  idl -> beg

  beg -> hld
  beg -> end

  hld -> end

  end -> idl
  end -> beg
}
```


### idl (idle)

Default phase (if no other conditions are met).


### beg (begin)

Predicate: `m_rdy && s_rdy`

`rdy`s are independent;
slave and master need not confer before asserting.

In this phase, `m_out` decides one transaction;
it contains orders that slave must execute.

Duration: `1 cycle`


### hld (hold)

Predicate: `s_don == 0`

Waiting for slave to finish request (see pipelining below).

Duration: optional, `0, 1, N, or ∞ cycles`


### end

Predicate: `s_don == 1`

Slave signals mission accomplished;
`s_out` is valid in this moment.

Duration: `1 cycle`


### Pipelining / Overlap

One may start a new transaction while an old one is still ongoing
(this should be logically deducible from the conditions of the phases).
But neither master nor slave is _obliged_ to accept it.


### Blocking / Non-blocking

It doesn't matter.
The protocol allows for both?

Example: master blocking, slave non-blocking.
Master can send a new request when 1) prev s_don, and 2) current s_rdy.



## Timing Diagram

https://wavedrom.com/editor.html

Protocol:
```
{signal: [
  {name: 'clk',   wave: 'p..|..'},
  {name: ''   ,   wave: '===|==', data: ['idl*', 'beg', 'hld*', 'end', 'idl*']},
  {},
  {name: 'm_rdy', wave: 'x1x|..'},
  {name: 'm_out', wave: 'x=x|..'},
  {},
  {name: 's_rdy', wave: 'x1x|..'},
  {name: 's_out', wave: 'x..|=x'},
  {name: 's_don', wave: 'x.0|1x'},
  ],
  config: {hscale: 2}
}
```

Simplified protocol (immediate response):
```
{signal: [
  {name: 'clk',   wave: 'p...'},
  {name: ''   ,   wave: '====', data: ['idl*', 'beg', 'end', 'idl*']},
  {},
  {name: 'm_rdy', wave: 'x1x.'},
  {name: 'm_out', wave: 'x=x.'},
  {},
  {name: 's_out', wave: 'x.=x'},
  ],
  config: {hscale: 2}
}
```
