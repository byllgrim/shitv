# Formal

Trying to comply with "rvfi".

https://github.com/SymbioticEDA/riscv-formal/blob/master/docs/rvfi.md


## Usage

1. Clone https://github.com/SymbioticEDA/riscv-formal
2. `ln -s blabla/riscv-formal/ rvfi`
3. `make`


## Definitions

XLEN = 32.
The size of `x` registers.

ILEN = 32.
Max instruction width.

NRET = 1
Max number of cycles that can retire in one cycle.


## Signals du Instructions

```
rvfi_valid - An instruction is retired
rvfi_order - The instruction's index
rvfi_insn  - The instruction word
rvfi_trap  - Instruction is illegal
rvfi_halt  - Last instruction before exec halts
rvfi_intr  - While running trap handlers
rvfi_mode  - Privilege level
rvfi_ixl   - MXL/SXL/UXL ???
```


## Signals du Registers

```
rvfi_rs1_addr  - The instruction's register address
rvfi_rs2_addr  - The instruction's register address
rvfi_rs1_rdata - Register content before the instruction
rvfi_rs2_rdata - Register content before the instruction
rvfi_rd_addr   - The instruction's rd address
rvfi_rd_wdata  - Register content after the instruction
```


## Signals du PC

```
rvfi_pc_rdata - PC before instruction
rvfi_pc_wdata - PC after instruction
```


## Signals du Memory

```
rvfi_mem_addr  - Accessed memory location
rvfi_mem_rmask - Byte lane enable
rvfi_mem_wmask - Byte lane enable
rvfi_mem_rdata - Content before the instruction
rvfi_mem_wdata - Content after the instruction
```


## TODO

Use all of these:
```
insn            20
reg       15    25
pc_fwd    10    30
pc_bwd    10    30
liveness  1  10 30
unique    1  10 30
causal    10    30
```
