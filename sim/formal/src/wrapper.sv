`default_nettype none

module rvfi_wrapper (
  input  clock,
  input  reset,
  `RVFI_OUTPUTS
);

  reg  delete_this_reg;

  // TODO instantiate DUT

  // Signals du instructions
  assign rvfi_valid = 0;
  assign rvfi_order = 0;
  assign rvfi_insn = 0;
  assign rvfi_trap = 0;
  assign rvfi_halt = 0;
  assign rvfi_intr = 0;
  assign rvfi_mode = 0;
  assign rvfi_ixl = 0;

  // Signals du registers
  assign rvfi_rs1_addr = 0;
  assign rvfi_rs2_addr = 0;
  assign rvfi_rs1_rdata = 0;
  assign rvfi_rs2_rdata = 0;
  assign rvfi_rd_addr = 0;
  assign rvfi_rd_wdata = 0;

  // Signals du PC
  assign rvfi_pc_rdata = 0;
  assign rvfi_pc_wdata = 0;

  // Signals du mem
  assign rvfi_mem_addr = 0;
  assign rvfi_mem_rmask = 0;
  assign rvfi_mem_wmask = 0;
  assign rvfi_mem_rdata = 0;
  assign rvfi_mem_wdata = 0;

endmodule
