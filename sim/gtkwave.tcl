proc addModuleSignals {module} {
    set numnets [gtkwave::getNumFacs]
    set signals [list]

    gtkwave::addCommentTracesFromList $module

    for {set i 0} {$i < $numnets} {incr i} {
        set netname [gtkwave::getFacName $i]
        if {[regexp $module $netname]} {
            lappend signals "$netname"
        }
    }

    gtkwave::addSignalsFromList $signals
}

proc main {} {
    addModuleSignals "dpram"
    addModuleSignals "regfile"
    addModuleSignals "ifstage"
    addModuleSignals "idstage"
    addModuleSignals "exstage"
    addModuleSignals "memstage"
    addModuleSignals "wbstage"
}

main
