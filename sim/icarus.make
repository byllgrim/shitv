ICARUS_FLAGS = -g2005 -Wall
ICARUS_SRC   = \
	icarus.v \
	$(RTL_SRC) \
	$(TB_SRC)

icarus.vvp: $(ICARUS_SRC)
	iverilog $(ICARUS_FLAGS) -o icarus.vvp $(ICARUS_SRC)

icarus-comp: icarus.vvp

icarus-runhex: icarus-comp hex
	vvp -N icarus.vvp +hex=$(HEX)

icarus-clean:
	rm -rf *.vvp *.vcd
