RTL_SRC    = ../rtl/*.v ../rtl/pipeline/*.v
TB_SRC     = ../bench/*.v
INC        = -I../rtl -I../rtl/pipeline -I../bench
HEX        = SPECIFY_A_HEX_FILE

default: comp

SIM     = verilator
include   verilator.make
include   icarus.make

comp:
	make $(SIM)-comp

runhex: $(SIM)-runhex

runall:
	make SIM=verilator boot
	make SIM=verilator test
	make SIM=icarus boot
	make SIM=icarus test

gui:
	gtkwave --script gtkwave.tcl trace.vcd

test:
	make runlist LIST="$$(     \
		find ../sw/rv32i   \
		| grep '.S$$'      \
		| sed 's/.S/.hex/' \
		| tr '\n' ' ')"

boot:
	make runlist LIST="$$(              \
		ls ../sw/bootstrap/auto/*.S \
		| sed 's/.S/.hex/'           \
		| tr '\n' ' ')"

runlist:
	rm -f test.log
	@for hex in $(LIST); do                        \
		make runhex HEX=$$hex >>test.log 2>&1; \
		if [ $$? -ne 0 ]; then                 \
			echo -n "FAIL ";               \
		else                                   \
			echo -n "OK   ";               \
		fi;                                    \
		echo $$hex;                            \
	done

hex:
	make -C ../sw $(HEX)

clean:
	make verilator-clean
	make icarus-clean
	rm -rf *.log *.hex
