#include <verilated.h>
#include <verilated_vcd_c.h>
#include <stdlib.h>

#include "Vtb.h"

static uint64_t tick;

enum {
    MAXTICKS = 10000,
    RSTTICK = 4,
};

const char *vcdfilename = "trace.vcd";

double
sc_time_stamp()
{
    return tick;
}

int
main(int argc, char *argv[])
{
    Vtb *tb;
    VerilatedVcdC *vcd;
    int exitcode;

    Verilated::commandArgs(argc, argv);

    tb  = new Vtb;

    Verilated::traceEverOn(true);
    vcd = new VerilatedVcdC;
    tb->trace(vcd, 99);
    vcd->open(vcdfilename);

    tb->clk = 0;
    for (tick = 0; ((tick <  MAXTICKS) && !Verilated::gotFinish()); tick++) {
        tb->clk = !tb->clk;
        tb->rst = (tick <= RSTTICK);

        tb->eval();
        vcd->dump(tick);
    }

    if (tick < MAXTICKS) {
        exitcode = tb->o_exitcode;
    } else {
        fprintf(stderr, "shitv: verilator timed out\n", exitcode);
        exitcode = EXIT_FAILURE;
    }

    tb->final();
    delete tb;
    vcd->close();
    return !!exitcode;
}
