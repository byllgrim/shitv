VERILATOR_FLAGS = -Wall --language 1364-2005 --cc --exe --trace
VERILATOR_SRC   = \
	../bench/tb.v \
	verilator.cpp

VERILATOR_EXE  = obj_dir/Vtb
VERILATOR_RAND = +verilator+rand+reset+2

obj_dir/Vtb.cpp: $(RTL_SRC) $(TB_SRC) $(VERILATOR_SRC)
	rm -f $@
	verilator $(VERILATOR_FLAGS) $(INC) $(VERILATOR_SRC)

obj_dir/Vtb: obj_dir/Vtb.cpp
	$(MAKE) -j 4 -C obj_dir -f Vtb.mk

verilator-comp: obj_dir/Vtb

verilator-runhex: verilator-comp hex
	$(VERILATOR_EXE) $(VERILATOR_RAND) +hex=$(HEX)

verilator-clean:
	rm -rf obj_dir *.vcd
