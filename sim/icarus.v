`default_nettype none

module icarus;
  reg                                                    clk;
  reg                                                    rst;

  // Hack, because icarus refuse to be sane about dumping arrays
  wire [31:0] regfile_x0 = tb.core.regfile.registers[0];
  wire [31:0] regfile_x1 = tb.core.regfile.registers[1];
  wire [31:0] regfile_x2 = tb.core.regfile.registers[2];
  wire [31:0] regfile_x3 = tb.core.regfile.registers[3];
  wire [31:0] regfile_x4 = tb.core.regfile.registers[4];

  tb tb (
      .clk(clk),
      .rst(rst)
  );

  initial begin
    $dumpfile("trace.vcd");
    $dumpvars();
  end

  initial begin
    rst = 1;
    #30;
    rst = 0;
  end

  initial begin
    clk = 0;

    forever #5 clk = !clk;
  end

  initial begin
    #10000;
    $display("simulation timeout");
    $stop;
  end
endmodule
