`default_nettype none

module dpram (
    input wire clk,
    input wire rst,

    input wire [31:0] i_a_raddr,
    input wire        i_a_rdy,

    input wire [31:0] i_b_raddr,
    input wire [31:0] i_b_waddr,
    input wire [31:0] i_b_wdata,
    input wire [ 2:0] i_b_siz,
    input wire        i_b_rdy,
    input wire        i_b_wen,

    output reg [31:0] o_a_rdata,
    output reg        o_a_don,

    output reg [31:0] o_b_rdata,
    output reg        o_b_don
);

  localparam ADDRWIDTH = 12;
  localparam BYTES = 2 ** ADDRWIDTH;
  localparam HALTADDR = 32'h7FF;

  reg     [   7:0] mem      [0:BYTES-1];



  // Load firmware file

  reg     [1023:0] filename;
  integer          i;

  initial begin
    if (!$value$plusargs("hex=%s", filename)) begin
      $display("no firmware");
      $stop;
    end

    for (i = 0; i < BYTES; i = i + 1) mem[i] = 8'hA5;

    $readmemh(filename, mem);
  end



  always @(posedge clk) begin : registeroutputs
    o_a_rdata <= 0;
    o_a_don <= 0;
    o_b_rdata <= 0;
    o_b_don <= 0;

    if (!rst) begin
      servereadrequests;
      storewriterequests;
      memdumponhalt;
    end
  end



  task automatic servereadrequests;
    begin
      if (i_a_rdy) begin
        o_a_rdata <= readdata(i_a_raddr);
        o_a_don <= 1;
      end
      if (i_b_rdy) begin
        o_b_rdata <= readdata(i_b_raddr);
        o_b_don <= 1;

        if (i_b_siz < 4) o_b_rdata[31:16] <= 0;
        if (i_b_siz < 2) o_b_rdata[15:8] <= 0;
      end
    end
  endtask



  task automatic storewriterequests;
    if (i_b_wen) begin
      if (i_b_siz >= 1) mem[i_b_waddr + 0] <= i_b_wdata[7:0];

      if (i_b_siz >= 2) mem[i_b_waddr + 1] <= i_b_wdata[15:8];

      if (i_b_siz >= 4) begin
        mem[i_b_waddr + 2] <= i_b_wdata[23:16];
        mem[i_b_waddr + 3] <= i_b_wdata[31:24];
      end
    end
  endtask



  function automatic [31:0] readdata;
    input [31:0] addr;
    reg [ADDRWIDTH-1:0] memindex;

    begin
      memindex = addr[ADDRWIDTH - 1:0];

      readdata[7:0] = mem[memindex + 0];
      readdata[15:8] = mem[memindex + 1];
      readdata[23:16] = mem[memindex + 2];
      readdata[31:24] = mem[memindex + 3];

      if (addr == 0);  // upper bits of addr is unused
    end
  endfunction



  integer fd;

  task automatic memdumponhalt;
    begin
      if (i_b_waddr == HALTADDR) begin
        fd = $fopen("memdump.hex", "w");
        if (fd == 0) $display("error: failed to open file");

        for (i = 0; i < BYTES; i = i + 4) begin
                $fdisplay(
                    fd,
                    "%h",
                    {mem[i + 3], mem[i + 2], mem[i + 1], mem[i + 0]}
                    );
            end

        $fflush(fd);
        $fclose(fd);
      end
    end
  endtask
endmodule
