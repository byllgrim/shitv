`default_nettype none

module tb (
    input wire clk,
    input wire rst,

    output wire [31:0] o_exitcode
);

  wire        dpram_a_don;
  wire        dpram_a_rdy;
  wire [31:0] dpram_a_raddr;
  wire [31:0] dpram_a_rdata;

  wire        dpram_b_don;
  wire        dpram_b_rdy;
  wire        dpram_b_wen;
  wire [ 2:0] dpram_b_siz;
  wire [31:0] dpram_b_raddr;
  wire [31:0] dpram_b_rdata;
  wire [31:0] dpram_b_waddr;
  wire [31:0] dpram_b_wdata;

  assign o_exitcode = dpram_b_wdata;


  // Instantiate RAM

  dpram dpram (
      .clk(clk),
      .rst(rst),

      .i_a_raddr(dpram_a_raddr),
      .i_a_rdy  (dpram_a_rdy),
      .i_b_raddr(dpram_b_raddr),
      .i_b_rdy  (dpram_b_rdy),
      .i_b_siz  (dpram_b_siz),
      .i_b_waddr(dpram_b_waddr),
      .i_b_wdata(dpram_b_wdata),
      .i_b_wen  (dpram_b_wen),
      .o_a_don  (dpram_a_don),
      .o_a_rdata(dpram_a_rdata),
      .o_b_don  (dpram_b_don),
      .o_b_rdata(dpram_b_rdata)
  );



  // Create a processor instance

  core core (
      .clk(clk),
      .rst(rst),

      .i_ram_a_don  (dpram_a_don),
      .i_ram_a_rdata(dpram_a_rdata),
      .i_ram_b_don  (dpram_b_don),
      .i_ram_b_rdata(dpram_b_rdata),
      .o_ram_a_raddr(dpram_a_raddr),
      .o_ram_a_rdy  (dpram_a_rdy),
      .o_ram_b_raddr(dpram_b_raddr),
      .o_ram_b_rdy  (dpram_b_rdy),
      .o_ram_b_siz  (dpram_b_siz),
      .o_ram_b_waddr(dpram_b_waddr),
      .o_ram_b_wdata(dpram_b_wdata),
      .o_ram_b_wen  (dpram_b_wen)
  );



  // Let software abort simulation

  always @(posedge clk) begin
    if (dpram_b_waddr == 32'h7FF) begin
      $display("simulation aborted by software: 0x%0h", o_exitcode);

      if (o_exitcode != 0) begin
        $stop();
      end else begin
        $finish();
      end
    end
  end
endmodule
