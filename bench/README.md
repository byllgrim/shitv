RTL sources needed for running tests.

Related info:
Test cases themselves are `.S` files in the `/sw` directory.
Simulation scripts are in the `/sim` directory.
