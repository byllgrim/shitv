# SHIT-V

Minimal RISC-V core (rv32i unprivileged).

Foci:
* Legibility  - Yes
* Minimalism  - Yes
* Performance - NO!



## Usage

I would not advice anyone to use this.

Run simulations in `sim/` directory (with `Verilator` or `Icarus`).



## Details

Language: Verilog 2005.

Directory hierarchy:
https://cdn.opencores.org/downloads/opencores_coding_guidelines.pdf

Lacks:
There are no interrupts nor exceptions,
FENCE and SYSTEM is not implemented,
the critical path is huge,
etc etc.

More details in `find . | grep -i readme`.



## Dependencies

(This list is not comprehensive)

* Verilator (or icarus)
  https://www.veripool.org/projects/verilator/wiki/Installing
* Icarus (or verilator)
  http://iverilog.icarus.com/
* Gnu toolchain
  https://github.com/riscv/riscv-gnu-toolchain
* (optionally)
  Yosys, *pnr, icetime
  http://www.clifford.at/icestorm/
